import { Component, OnInit } from '@angular/core';
import { Product } from 'src/app/models/Product';
import { PanierService } from 'src/app/services/panier.service';
import { ProductService } from 'src/app/services/product.service';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit {

  productsList : Product[] = []

  constructor(private _productService : ProductService, private _panierService : PanierService) {

  }

  ngOnInit(): void {
      this.productsList = this._productService.getAll()
  }

  addToCart(product : Product) {
    this._panierService.addProduct(product)
  }
}
